# README #

This is to predict optimal steady state LAI for Australia. 

This is the final version intented for JAMSE paper: Yang et al., 2018 "Applying the concept of ecohydrological equilibrium to predict steady-state leaf area index "

Just go to main.R and click source should get the results needed and figures plotted. However, TERN data is used and they require each user to register and download. Details can be found in r/process_tern.r.

Please contact Jinyan Yang via email jyyoung at live dot com or leave comments on the page. 