if(!file.exists("output/data/result.ellis.rds")){
  source("R/process_ellis.R")
}else{
  result.ellis <- readRDS("output/data/result.ellis.rds")
}

if(!file.exists("output/data/result.iio.rds")){
  source("R/process_iio.R")
}else{
  result.iio <- readRDS("output/data/result.iio.rds")
}

if(!file.exists("output/data/result.tern.rds")){
  source("R/process_tern.R")
}else{
  result.tern <- readRDS("output/data/result.tern.rds")
}


# cbind all data
result.tern$from <- rep("tern",nrow(result.tern))
result.iio$from <- rep("iio",nrow(result.iio))
result.ellis$from <- rep("ellis",nrow(result.ellis))
subset(result.tern,select=c(-site))
temp.bind <- rbind(result.tern,result.ellis)
result.all <- rbind(temp.bind,result.iio)
result.all$Latitude <-  c(tern.annual$Latitude,ellis.data$Latitude,iio.aust$Latitude)
result.all$Longitude <- c(tern.annual$Longitude,ellis.data$Longitude,iio.aust$Longitude)

# modis 
result.all$modis <- get.modis.for.coord.func(result.all$Longitude,result.all$Latitude)
# result.all$cyc <- get.modis.for.coord.func(result.all$Longitude,result.all$Latitude,
#                                            modis.sub = readRDS(file.path("cache","cyc.sub.rds")))
# 
pet <- readRDS("cache/pet.rds") * 12 
# coord for vpd
lati <- ncvar_get(nc_open("downloads/rainfall/rainfall2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/rainfall/rainfall2002.nc"), "longitude")
# 
latitude <- result.all$Latitude 
longitude <- result.all$Longitude
num.lati <- c()
for (i in 1:length(latitude)){
  num.lati[i] <- which(abs(lati-latitude[i])==min(abs(lati-latitude[i])))
}

num.longi <- c()
for (i in 1:length(longitude)){
  num.longi[i] <- which(abs(long-longitude[i])==min(abs(long-longitude[i])))
}
pet.site <- c()

for (i in 1:length(num.longi)){
  pet.site[i] <- pet[num.longi[i],num.lati[i]]
}


result.all$PET <- pet.site
result.sumall <- summaryBy(LAI + measured + modis + VPD + PAR + TMAX + PET ~ MAP,
                           data=result.all,FUN = c(mean,sd),na.rm = TRUE,id = ~from + site +Latitude + Longitude)
# remove doggy points
result.sumall <- result.sumall[result.sumall$measured.mean < 8,]

saveRDS(result.sumall,"output/data/inSituData.rds")
        
saveRDS(result.all,"output/data/inSituDataall.rds")
