# the data change link on nci very frequently
# check whether the ones here work first

# Tmax and vp are on monthly
# PPT and rad are annual data.
# the link for tmax and vp are so different that I used different functions to do it
##############################################################################################
##############################################################################################
##############################################################################################
##############################################################################################
# #download tmax monthly
download.var <- function(year,outpath="downloads/tmax/",var.name = "tmax",baseUrl,var.long.nm){
  
  temp <- list()
  #download monthly data
  for (i in 1:12){
    file <- paste0(year,sprintf("%02d", i),".nc")
    
    url <- paste0(baseUrl,file)
    
    writeName <- paste0(outpath,var.name,year,i,".nc")
    
    curl_download_withtest(writeName, url)
    
    ncR <- nc_open(file.path(writeName))
    
    temp[[i]] <- ncvar_get(ncR, var.long.nm)
  }
  array <- abind(temp, along=3)
  # Annual average VP for every x,y location
  annual <- rowMeans(array, dims=2)
  return(annual)
}

# get t function
get.t <- function(sim_years,var.nm,...){
  
  for (year in sim_years){
    # download.var(year,outpath=path,var.name = var.name,...)
    
    fn <- file.path("cache", sprintf(paste0(var.nm,"%s.rds"),year))
    
    if(!file.exists(fn)){
      path <- paste0("downloads/",var.nm,"/")
      dir.create(path, showWarnings=FALSE)
      temp <- download.var(year,outpath=path,var.name = var.nm,...)
      saveRDS(temp, fn)
    } else {
      message("Skipped - ", fn, "; already exists.")
    } 
    
  }
}

#download VP
downloadVP <- function(year, outpath="downloads/VP"){
  dir.create(outpath, showWarnings=FALSE)
  tempVPD <- list()
  #download monthly data
  for (i in 1:12){
    baseUrl <- paste0("http://dapds00.nci.org.au/thredds/fileServer/rr9/",
                      "ANUClimate/ANUClimate_v1-0_vapour-pressure_monthly_0",
                      "-01deg_1970-2012/00000000/ANUClimate_v1-0_vapour-",
                      "pressure_monthly_0-01deg_1970-2012_00000000_")

    file <- paste0(year,sprintf("%02d", i),".nc")
    url <- paste0(baseUrl,file)
    
    writeName <- paste0(outpath,"/VP",year,i,".nc")
    
    curl_download_withtest(writeName, url)
  }
}

#function to  downloaded file from url with test 
curl_download_withtest <- function(filename, url,...){
  if(!file.exists(filename)){
    curl_download(url,filename,quiet = FALSE,...)
  } else {
    message("Skipped ",filename, "; already exists.")
  }
}
#read downloaded VP
#get annual VPd from monthly data
readVP <- function(year, path="downloads/VP"){
  # read vp files 
  files <- list.files(path = path, pattern = paste0("VP",year))
  files <- files[grep("[.]nc", files)]
  temp.VPD <- list()
  # read tmax files 
  files.t <- list.files(path = "downloads/temperature", pattern = paste0("temperature",year))
  files.t <- files.t[grep("[.]nc", files.t)]
  
  # get monthly average vpd
  for (i in 1:12){
    ncR.t <- nc_open(file.path("downloads/temperature",files.t[i]))
    temp.t <- ncvar_get(ncR.t, "air_temperature")
    ncR <- nc_open(file.path(path,files[i]))
    temp.vp <- ncvar_get(ncR, "vapour_pressure") / 10 # divided by 10 to convert from bar to kpa
    vpd.m <- (0.6108 * exp(17.27 * temp.t / (temp.t + 237.3))) / temp.vp
    temp.VPD[[i]] <- vpd.m
  }
  
  vpd_array <- abind(temp.VPD, along=3)
  # Annual average VPd
  vpd_annual <- rowMeans(vpd_array, dims=2)
  
  return(vpd_annual)
}

get.vpd <- function(years){

  for (year in years){
    # doenload vp from emast
    downloadVP(year)
    fn <- file.path("cache", sprintf("vpd%s.rds",year))
    # calculate monthly VPD based on vp and tmax
    if(!file.exists(fn)){
      VPtemp <- readVP(year)
      saveRDS(VPtemp, fn)
    } else {
      message("Skipped - ", fn, "; already exists.")
    } 
  }
  
}

# get long term mean
getMean <- function(variable, path="cache/"){
  files <- list.files(path = path, pattern = paste0(variable))
  files <- files[grep("[.]rds", files)]
  temp <- list()
  
  for (i in 1:length(files)){
    temp[[i]] <- readRDS(file.path(path,files[i]))
  }
  temp.array <- abind::abind(temp, along=3)
  # Annual average  for every x,y location
  average <- rowMeans(temp.array, dims=2)
  
  return(average)
}

get20mean <- function(var.name){
  file.name <- sprintf("%s.rds",var.name)
  if(!file.exists(file.path("cache",file.name))){
    rain.mean <- getMean(var.name)
    saveRDS(rain.mean, file.path("cache",file.name))
  } else {
    message(file.name,"; already exists.")
  }
}

# get annual PPT and rad Data
get_variable_eMAST <- function(variable,year,outpath="downloads",url){
  
  # baseUrl <- paste0("http://dapds00.nci.org.au/thredds/fileServer/rr9/",
  #                   "Climate/eMAST/eMAST_R_Package/0_01deg/v1m1_aus/yr/land/")
  # 
  # rUrl <- paste0(variable,"/e_01/1970_2012/")
  # 
  # if (variable == "pavg") modifier = "eMAST_R_Package_yr_" else modifier = "eMAST_yr_"
  # 
  # filename <- paste0(modifier,variable,"_v1m1_",year,".nc")
  # 
  # url <- paste0(baseUrl,rUrl,filename)
  
  basefile <- paste0(variable,year,".nc")
  
  writeName <- file.path(outpath,variable,basefile)
  
  curl_download_withtest(writeName, url)
}

#function to open downloaded file
open_variable_eMAST <- function(fileName,fullName){
  
  ncR <- nc_open(fileName)
  test <- ncvar_get(ncR, fullName)
  
  return(test)
}

# get ppt and arad func
get.met <- function(sim_years,
                    variable.all = c("arad","pavg"),
                    variable.fullNameList = c("annual_mean_radiation",
                                               "lwe_thickness_of_precipitation_amount"),...){
  variableList <- variable.all
  fullNameList <- variable.fullNameList
  
  for (i in seq_along(variableList)){
    for (year in sim_years){
      dir.create(file.path("downloads", variableList[i]), showWarnings=FALSE)
      # download from emast
      get_variable_eMAST(variableList[i],year,...)
      basefile <- paste0(variableList[i],year,".nc")
      fileName <- file.path("downloads",variableList[i],basefile)
      fileout <- file.path("cache", gsub("[.]nc",".rds", basefile))
      # open the downloaded files and save as rds
      if(!file.exists(fileout)){
        tmpvar <- open_variable_eMAST(fileName, fullNameList[i])
        saveRDS(tmpvar, fileout)
      } else {
        message("Skipped - ", fileout, "; already exists")
      }
    }
  }
}


