library(ncdf4)
library(curl)
library(abind)
library(raster)
library(foreach)
library(parallel)
library(doParallel)
library(doBy)
library(mgcv)
library(stringr)
library(readxl)
library(XLConnect)
library(plantecophys)

if("g1.opt.func" %in% rownames(installed.packages()) == FALSE){
  library("devtools")
  install_bitbucket("Jinyan_Jim_Yang/g1.opt.package.git")
}

library("g1.opt.func")

if(!dir.exists("downloads"))dir.create("downloads")
if(!dir.exists("downloads/radiation"))dir.create("downloads/radiation")
if(!dir.exists("downloads/LCM"))dir.create("downloads/LCM")
if(!dir.exists("downloads/modis"))dir.create("downloads/modis")
if(!dir.exists("downloads/modis/qa"))dir.create("downloads/modis/qa")
if(!dir.exists("cache"))dir.create("cache")
if(!dir.exists("output"))dir.create("output")
if(!dir.exists("output/data"))dir.create("output/data")
if(!dir.exists("output/data/eCa"))dir.create("output/data/eCa")
if(!dir.exists("output/data/trans"))dir.create("output/data/trans")
if(!dir.exists("output/data/340"))dir.create("output/data/340")
if(!dir.exists("output/data/389"))dir.create("output/data/389")


file.nm <- list.files("r/",pattern = "function_")
for(i in file.nm){
  source(paste0("r/",i))
}


